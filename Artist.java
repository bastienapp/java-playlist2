public class Artist {

    private String firstName;
    private String lastName;

    public Artist(
        String firstName,
        String lastName
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // méthodes
    public String getFullName() {
        return this.lastName + " " + this.firstName;
    }

    // getters / setters
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String newFirstName) {
        this.firstName = newFirstName;
    }
}
