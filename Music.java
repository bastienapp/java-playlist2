import java.util.Set;

public class Music {

    private String title;
    private int duration;

    // List : liste d'éléments qui se suivent avec un index, on peut modifier la taille de la liste (ajouter/supprimer des éléments)
    // Set : un ensemble d'éléments (ils ne se suivent pas), on ne peut pas avoir deux fois le même éléments
    private Set<Artist> artistSet; // String[], Artist[], List<Artist>, Set<Artist>

    public Music(
        String title,
        int duration,
        Set<Artist> artistSet
    ) {
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    public void addArtist(Artist artist) {
        this.artistSet.add(artist);
    }

    public String getInfos() {
        /*
        // design pattern Builder
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.title);
        stringBuilder.append(" ");
        stringBuilder.append(Helper.formatDuration(this.duration));
        return stringBuilder.toString();
        */
        String artistNames = "";
        for (Artist eachArtist : artistSet) {
            artistNames += " " + eachArtist.getFullName();
        }
        return this.title + " " + Helper.formatDuration(this.duration) + artistNames;
        // return String.format("%s (%s) -%s", this.title, Helper.formatDuration(this.duration), artistNames);
    }

    public int getDuration() {
        return this.duration;
    }
}
