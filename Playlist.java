import java.util.ArrayList;
import java.util.List;

public class Playlist {

    private Music currentMusic;
    private List<Music> musicList = new ArrayList<>(); // la liste est vide (sinon elle aurait été nulle)

    // ici je laisse un constructeur vide par défaut

    public void add(Music music) {
        this.musicList.add(music);
    }

    public void remove(int position) {
        this.musicList.remove(position);
    }

    public String getTotalDuration() {

        int total = 0;
        for (var music : musicList) {
            total += music.getDuration();
        }

        return Helper.formatDuration(total);
    }

    public void next() {
        // on pourrait faire autrement en cherchant la position de la musique en cours et en passant à la suivante
        if (!this.musicList.isEmpty()) {
            this.currentMusic = this.musicList.get(0);
            this.remove(0); // supprime la musqie en première position
            System.out.println("Lecture de " + currentMusic.getInfos());
        } else {
            System.out.println("Pas de musique trouvée");
        }
    }
}
