public class Helper {

    public static String formatDuration(int duration) {
        int minutes = duration / 60; // 235 / 60 == 3
        int seconds = duration % 60; // 55

        return minutes + ":" + (seconds < 10 ? "0" + seconds : seconds);

        // condition ? valeur si condition vraie : valeurs si condition fausse
    }
}
