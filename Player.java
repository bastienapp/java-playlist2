import java.util.ArrayList;
import java.util.HashSet;

class Player {

    public static void main(String[] args) {
        Artist davidGuetta = new Artist("David", "Guetta");
        Artist ladyGaga = new Artist("Lady", "Gaga");
        davidGuetta.setFirstName("Michel");
        System.out.println(davidGuetta.getFullName());

        HashSet<Artist> artistMusic1 = new HashSet<>();
        artistMusic1.add(davidGuetta); // ajoute un élément à une liste
        artistMusic1.add(ladyGaga);

        HashSet<Artist> artistMusic2 = new HashSet<>();
        artistMusic2.add(davidGuetta);

        // System.out.println(listTest.size()); // récupère le nombre d'éléments
        Music bloodyMary = new Music("Bloody Mary", 235, artistMusic1);
        Music sunday = new Music("Sunday", 184, artistMusic2);

        System.out.println(bloodyMary.getInfos());

        Playlist myFavourites = new Playlist();
        myFavourites.add(bloodyMary); // index (ou position) 0
        myFavourites.add(sunday); // index (ou position) 1

        // myFavourites.remove(0);

        System.out.println(myFavourites.getTotalDuration());

        myFavourites.next();
        myFavourites.next();
        myFavourites.next();
    }
}